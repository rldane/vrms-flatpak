# vrms-flatpak

## A "Virtual Richard M. Stallman" for Flatpak packages

This script is loosely based on (inspired by) the **[vmrs \(Virtual Richard M. Stallman\)](https://debian.pages.debian.net/vrms/)** package created for Debian and its derivatives. Similar to how vmrs alerts users of Debian and Debian-derived distros of the presence of contrib or non-free software on their machine, this script attempts to parse the license information for every flatpak installed on the system, and report which packages are free, proprietary, or unknown (or don't have any license information in their package metadata -- boo, hiss!).

Since vrms-flatpak is oriented around flatpak itself, and not dpkg/apt, it should work on any linux distro (or any UNIX-y OS) that has flatpak.

Please notify me if this package doesn't work for you. I'd love to extend it further. Right now, it only recognizes a handful of licenses as FLOSS, and only licenses including the words 'proprietary' or 'commercial' as non-free.

The script only requires bash and basic binutils (and of course, flatpak). It will query flatpak using `flatpak list` and `flatpak info $package` for license information.
